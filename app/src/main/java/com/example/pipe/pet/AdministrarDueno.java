package com.example.pipe.pet;

import java.util.ArrayList;

/**
 * Created by Pipe on 19-12-2017.
 */

public class AdministrarDueno {

    private static AdministrarDueno instance;
    private ArrayList<Dueno> values;

    protected AdministrarDueno() { values = new ArrayList<>(); }

    public static AdministrarDueno getInstance() {
        if (instance == null) {
            instance = new AdministrarDueno();
        }
        return instance;
    }

    public boolean guardarDueno(Dueno dueno) {
        if(!buscarDueno(dueno)) {
            values.add(dueno);
            return true;
        }else {
          return false;
        }
    }

    private boolean buscarDueno(Dueno dueno) {
        for (Dueno a : values) {
            if (dueno.getNombre().equals(a.getNombre())) {
                return true;
            }
        }
        return false;
    }

    public ArrayList<Dueno> listarDueno() {
        return values;
    }
}

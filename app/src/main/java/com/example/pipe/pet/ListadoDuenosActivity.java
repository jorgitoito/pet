package com.example.pipe.pet;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

public class ListadoDuenosActivity extends AppCompatActivity {

    private ListView lvDuenos;
    private ListadoDuenosAdapter listadoDuenosAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_duenos);
        lvDuenos = (ListView)findViewById(R.id.lvDuenos);
        listadoDuenosAdapter = new ListadoDuenosAdapter(this, AdministrarDueno.getInstance().listarDueno());
        lvDuenos.setAdapter(listadoDuenosAdapter);
    }
}

package com.example.pipe.pet;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

public class ListadoCitasActivity extends AppCompatActivity {

    private ListView lvCitas;
    private ListadoCitasAdapter listadoCitasAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lvCitas = (ListView)findViewById(R.id.lvCitas);
        listadoCitasAdapter = new ListadoCitasAdapter(this, AdministrarCita.getInstance().listarCita());
        lvCitas.setAdapter(listadoCitasAdapter);
    }
}

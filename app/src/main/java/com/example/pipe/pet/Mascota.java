package com.example.pipe.pet;

/**
 * Created by Pipe on 18-12-2017.
 */

public class Mascota {
    private String nombre;
    private String raza;
    private String porte;
    private Dueno dueno;
    private String fotoAntes1;
    private String fotoAntes2;
    private String fotoDespues1;
    private String fotoDespues2;

    public Mascota(String nombre, String raza, String porte, Dueno dueno) {
        this.nombre = nombre;
        this.raza = raza;
        this.porte = porte;
        this.dueno = dueno;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRaza() {
        return raza;
    }

    public void setRaza(String raza) {
        this.raza = raza;
    }

    public String getPorte() {
        return porte;
    }

    public void setPorte(String porte) {
        this.porte = porte;
    }

    public Dueno getDueno() {
        return dueno;
    }

    public void setDueno(Dueno dueno) {
        this.dueno = dueno;
    }
}

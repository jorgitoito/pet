package com.example.pipe.pet;

/**
 * Created by Pipe on 18-12-2017.
 */

public class Dueno {
    private String nombre;
    private String telefono;
    private String direccion;
    private String descripcion;

    public Dueno(String nombre, String telefono, String direccion, String descripcion) {
        this.nombre = nombre;
        this.telefono = telefono;
        this.direccion = direccion;
        this.descripcion = descripcion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}

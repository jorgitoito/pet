package com.example.pipe.pet;

import java.sql.Time;
import java.util.Date;

/**
 * Created by Pipe on 18-12-2017.
 */

public class Cita {
    private Date fecha;
    private Time hora;
    private Mascota mascota;

    public Cita(Date fecha, Time hora, Mascota mascota) {
        this.fecha = fecha;
        this.hora = hora;
        this.mascota = mascota;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Time getHora() {
        return hora;
    }

    public void setHora(Time hora) {
        this.hora = hora;
    }

    public Mascota getMascota() {
        return mascota;
    }

    public void setMascota(Mascota mascota) {
        this.mascota = mascota;
    }
}

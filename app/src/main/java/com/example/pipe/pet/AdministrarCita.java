package com.example.pipe.pet;

import java.util.ArrayList;

/**
 * Created by Pipe on 19-12-2017.
 */

public class AdministrarCita {

    private static AdministrarCita instance;
    private ArrayList<Cita> values;

    protected AdministrarCita() { values = new ArrayList<>(); }

    public static AdministrarCita getInstance() {
        if (instance == null) {
            instance = new AdministrarCita();
        }
        return instance;
    }

    public boolean guardarCita(Cita cita) {
        //if(!buscarHora(hora)) {
        values.add(cita);
        return true;
        //}else {
        //  return false;
        //}
    }

    private boolean buscarCita(Cita cita) {
        for (Cita a : values) {
            if (cita.equals(a)) {
                return true;
            }
        }
        return false;
    }

    public ArrayList<Cita> listarCita() {
        return values;
    }
}

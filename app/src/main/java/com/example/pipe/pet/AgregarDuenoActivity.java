package com.example.pipe.pet;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class AgregarDuenoActivity extends AppCompatActivity {

    private Button btnAgregar;
    private EditText edNombre,edTelefono,edDireccion,edDescripcion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar_dueno);

        btnAgregar = (Button) findViewById(R.id.btnGuardar);
        edNombre = (EditText) findViewById(R.id.edNombre);
        edTelefono = (EditText) findViewById(R.id.edTelefono);
        edDireccion = (EditText) findViewById(R.id.edDireccion);
        edDescripcion = (EditText) findViewById(R.id.edDescripcion);

        btnAgregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validarCampos()){
                    if(AdministrarDueno.getInstance().guardarDueno(obtenerDueno())){
                        Toast.makeText(AgregarDuenoActivity.this, "Grabo dueño!", Toast.LENGTH_SHORT).show();
                        limpiarTxt();
                        Intent i = new Intent(AgregarDuenoActivity.this, MainActivity.class);
                        startActivity(i);
                    } else {
                        Toast.makeText(AgregarDuenoActivity.this, "Existe este telefono", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    private boolean validarCampos() {
        String nombre = edNombre.getText().toString();
        String direccion= edDireccion.getText().toString();
        String telefono = edTelefono.getText().toString();

        if(nombre.isEmpty()){
            Toast.makeText(this, "Ingrese un nombre", Toast.LENGTH_SHORT).show();
            return false;
        }
        if(direccion.isEmpty()){
            Toast.makeText(this, "Ingrese una dirección", Toast.LENGTH_SHORT).show();
            return false;
        }
        if(telefono.isEmpty()){
            Toast.makeText(this, "Ingrese un telefono valido", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private Dueno obtenerDueno(){
        return new Dueno(getTextET(edNombre),getTextET(edTelefono),getTextET(edDireccion),getTextET(edDescripcion));
    }

    private String getTextET(EditText et) {
        return et.getText().toString();
    }

    private void limpiarTxt() {
        edDireccion.setText("");
        edDescripcion.setText("");
        edTelefono.setText("");
        edNombre.setText("");
    }
}

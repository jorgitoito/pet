package com.example.pipe.pet;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;

/**
 * Created by Pipe on 21-12-2017.
 */

public class ListadoDuenosFragment extends Fragment{

    private ListView lvDuenos;
    private ListadoDuenosAdapter listadoDuenosAdapter;

    public static ListadoDuenosFragment newInstance(String text)
    {
        ListadoDuenosFragment f = new ListadoDuenosFragment();
        Bundle b = new Bundle();
        b.putString("msg", text);
        f.setArguments(b);
        return f;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.listado_duenos_fragment,container,false);
        lvDuenos = (ListView)view.findViewById(R.id.lvDuenos);
        listadoDuenosAdapter = new ListadoDuenosAdapter(getActivity(), AdministrarDueno.getInstance().listarDueno());
        lvDuenos.setAdapter(listadoDuenosAdapter);
        return view;
    }
}

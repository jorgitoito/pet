package com.example.pipe.pet;

import java.util.ArrayList;

/**
 * Created by Pipe on 19-12-2017.
 */

public class AdministrarMascota {

    private static AdministrarMascota instance;
    private ArrayList<Mascota> values;

    protected AdministrarMascota() { values = new ArrayList<>(); }

    public static AdministrarMascota getInstance() {
        if (instance == null) {
            instance = new AdministrarMascota();
        }
        return instance;
    }

    public boolean guardarMascota(Mascota mascota) {
        values.add(mascota);
        return true;
    }

    private boolean buscar(Mascota mascota) {
        for (Mascota a : values) {
            if (mascota.equals(a)) {
                return true;
            }
        }
        return false;
    }

    public ArrayList<Mascota> listarMascota() {
        return values;
    }
}

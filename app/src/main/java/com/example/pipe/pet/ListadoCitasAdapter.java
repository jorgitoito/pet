package com.example.pipe.pet;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Pipe on 21-12-2017.
 */

public class ListadoCitasAdapter extends ArrayAdapter<Cita>{

    private final List<Cita> list;
    private final Context context;

    public ListadoCitasAdapter(Context context, List<Cita> list) {
        super(context, R.layout.items_citas, list);
        this.context = context;
        this.list = list;
    }

    static class ViewHolder {
        protected TextView nombre, fecha, hora, direccion;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ListadoCitasAdapter.ViewHolder holder;
        if (convertView == null) {
            LayoutInflater inflator = LayoutInflater.from(context);
            convertView = inflator.inflate(R.layout.items_citas, null);
            holder = new ListadoCitasAdapter.ViewHolder();
            holder.nombre = (TextView) convertView.findViewById(R.id.tvNombre);
            holder.fecha = (TextView) convertView.findViewById(R.id.tvFecha);
            holder.hora = (TextView) convertView.findViewById(R.id.tvHora);
            holder.direccion = (TextView) convertView.findViewById(R.id.tvDireccion);
            convertView.setTag(holder);
        }else {
            holder = (ListadoCitasAdapter.ViewHolder) convertView.getTag();
        }

        Cita cita= getItem(position);

        holder.nombre.setText(cita.getMascota().getDueno().getNombre());
        holder.fecha.setText(cita.getFecha().toString());
        holder.hora.setText(cita.getHora().toString());
        holder.direccion.setText(cita.getMascota().getDueno().getDireccion());

        return convertView;
    }
}

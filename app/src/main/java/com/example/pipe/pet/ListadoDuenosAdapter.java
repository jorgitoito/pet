package com.example.pipe.pet;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Pipe on 19-12-2017.
 */

public class ListadoDuenosAdapter extends ArrayAdapter<Dueno>{

    private final List<Dueno> list;
    private final Context context;

    public ListadoDuenosAdapter(Context context, List<Dueno> list) {
        super(context, R.layout.items, list);
        this.context = context;
        this.list = list;
    }

    static class ViewHolder {
        protected TextView nombre, telefono, direccion;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        if (convertView == null) {
            LayoutInflater inflator = LayoutInflater.from(context);
            convertView = inflator.inflate(R.layout.items, null);
            holder = new ViewHolder();
            holder.nombre = (TextView) convertView.findViewById(R.id.tvNombre);
            holder.telefono = (TextView) convertView.findViewById(R.id.tvTelefono);
            holder.direccion = (TextView) convertView.findViewById(R.id.tvDireccion);
            convertView.setTag(holder);
        }else {
            holder = (ViewHolder) convertView.getTag();
        }

        Dueno dueno = getItem(position);

        holder.nombre.setText(dueno.getNombre());
        holder.telefono.setText(dueno.getTelefono());
        holder.direccion.setText(dueno.getDireccion());

        return convertView;
    }
}
